---
output:
  md_document:
    variant: markdown_github
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, echo = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "README-"
)
```

# RUcausal

The goal is to package is to provide implementations for causal inference algorithms developed in the Radboud University Causality group. The package contains an implementation of the [BCCD algorithm](https://www.auai.org/uai2012/papers/234.pdf).

# Installation

The package has been built on Linux using the [GNU Compiler Collection](https://gcc.gnu.org/). To install the package, open an R instance and run:

```
install.packages('devtools') # required package
devtools::install_git('https://gitlab.science.ru.nl/gbucur/RUcausal')
```

