#' Update skeleton after all level K tests are completed. (BCCD-stable)
#'
#' @param loci_pmax Numeric vector of the maximum probability for each
#' causal logic statement.
#' @param skeleton Numeric matrix of edge weights.
#' (skeleton > min_p = graph skeleton)
#'
#' @return Skeleton updated with the information in loci_pmax.
update_skeleton <- function(loci_pmax, skeleton) {
  D <- nrow(skeleton) # number of variables

  # Loop over all edges x-y with x < y
  for (x in 1:(D - 1)) {
    for (y in (x + 1):D) {
      # get corresponding logiacl statement in loci_pmax matrix
      # [x, x, z] = no edge x-z: c = 0; x < z
      idx <- x + (x - 1) * D + (y - 1) * D * D
      prob <- loci_pmax[idx, 1] # reliability estimate

      # Update skeleton
      skeleton[x, y] <- skeleton[y, x] <- 1 - prob # p(edge) = 1 - p(no edge)
    }
  }

  # check changes and return new skeleton
  # list(G = new_G, changed = any(G != new_G))

  skeleton
}
