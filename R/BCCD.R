#' Bayesian Constrained-based Causal Discovery
#'
#' \code{BCCD} is an implementation of the \emph{Bayesian constraint-based causal discovery} \insertCite{claassen2012bayesian}{RUcausal} algorithm.
#'
#'
#' @param R Data correlation matrix.
#' @param N Number of observations.
#' @param runmode A list of configuration parameters. See \code{\link[RUcausal]{list_algorithm_parameters}}.
#' @param prior_BGe Prior over causal structures for computing BGe score.
#' @seealso \code{\link[RUcausal]{update_prior_with_bkg_info}}.
#' @param max_K Maximum size of subsets to score (default is five).
#' @param bkg_info List of background information, containing two adjacency
#' matrices specifying `causal` and `structural` information. The causal
#' background information is encoded as:
#' \preformatted{
#' bkg_info$causal[i, j] ==
#'   | -1 : V_i /> V_j (there is no causal relation from V_i to V_j)
#'   | -p : like -1, but with confidence / probability p > 0
#'   |  0 : no info
#'   |  p : like +1, but with confidence / probability p > 0
#'   | +1 : V_i => V_j (there is a causal relation from V_i to V_j)
#' }
#' The structural background information is encoded as:
#' \preformatted{
#' bkg_info$structural[i, j] ==
#'   | -1 : there is no edge between i and j in the output PAG
#'   |  0 : no info
#'   | +1 : there is an edge between i and j in the output PAG
#' }
#' @param no_selection_bias Logical flag (default FALSE) indicating whether we
#' should disregard the possiblity of selection bias in our data.
#' @param ASP Logical flag (default FALSE) indicating whether the processing of
#' individual causal statements should be done using a (still experimental)
#' answer set programming solution.
#' @param provide_detailed_output Logical flag indicating whether more detailed
#' output, which includes the full list of inferred logical, causal and skeleton
#' statements should be returned.
#' @param save_intermediate_output Logical flag indicating whether intermediate
#' output should be saved for debugging purposes. The output is saved in the
#' file specified by runmode$output_file (see \code{\link[RUcausal]{list_algorithm_parameters}}).
#'
#' @return List containing the (detailed) output of BCCD.
#' \item{PAG}{Adjacency matrix description of a \emph{partial ancestral graph} (PAG) encoded as:
#'
#'   \code{P[i, j]: 0 = no edge, 1 = tail, 2 = arrowhead, 3 = circle}.
#'
#'   e.g. \eqn{P[i, j] = 2 and P[j, i] = 3 => i <-o j}
#' }
#'
#' \item{causal_matrix}{Matrix containing the inferred probabilities of causal statements encoded as:
#'
#'   \code{M[i, j]: 0 = do not know, +1 = i --> j, -1 = i -/> j}
#'
#'   e.g. \eqn{M[i, j] = 0.8 => p(i --> j) = 0.8, and M[i, j] = -0.8 => p(i -/> j) = 0.8}
#' }
#' #TODO: update probability to reliability
#' \item{reliability_tables}{List containing summary probability matrices.
#'
#'   \itemize{
#'     \item \code{edge}: E[i, j] (= E[j, i]) is the probability of an edge between i and j
#'     \item \code{tail}: T[i, j] is the probability of a tail mark at i on the (i, j) edge
#'     \item \code{arrowhead}: A[i, j] is the probability of an arrowhead mark at i on the (i, j) edge
#'     \item \code{selbias}: probability of selection bias
#'   }
#' }
#'
#' The next elements can be added to the return list by setting the parameter
#' provide_detailed_output to TRUE:
#'
#' \item{prob_L_use}{matrix of prob_L statements used in run_bayesian_loci =>
#'
#'     \code{[prior, idx, c-type, x-from, y-to, z-to, rule-origin,{+2-5 nodes}]}
#' }
#' \item{prob_L_max}{full list of prob_L statements found in BGe stage}
#' \item{trace_L}{list of inferred causal statements (for traceability)}
#' \item{trace_C}{list of inferred skeleton statements for the causal PAG}
#'
#' @export
#'
#' @examples
#' # 1a. Generate data from a simple v-structure (X -> Z <- Y)
#' N <- 10000 # number of observations
#' X <- rnorm(N)
#' Y <- rnorm(N)
#' Z <- X + Y + rnorm(N)
#' R <- cor(cbind(X, Y, Z), cbind(X, Y, Z)) # correlation matrix
#'
#' bccd.fit1a <- BCCD(R, N)
#' print(bccd.fit1a$PAG) # Print adjacency matrix of inferred graph
#' plot_PAG(bccd.fit1a$PAG) # Plot resulting partial ancestral graph (PAG)
#'
#' # 1b. Generate data from the LCD pattern (X -> Y -> Z) and use background knowledge
#' N <- 10000 # number of observations
#' X <- rnorm(N)
#' Y <- X + rnorm(N)
#' Z <- Y + rnorm(N)
#' R <- cor(cbind(X, Y, Z), cbind(X, Y, Z)) # correlation matrix
#'
#' bccd.fit1b <- BCCD(R, N)
#' print(bccd.fit1b$PAG) # Print adjacency matrix of inferred graph
#'
#' # Now we incorporate the background knowledge that X is not caused by Y or Z
#' bkg_info <- list(
#' structural = NULL,
#' causal = matrix(c(
#'    0, 0, 0,
#'   -1, 0, 0,
#'   -1, 0, 0
#' ), nrow = 3, ncol = 3, byrow = TRUE))
#'
#' bccd.fit1b_with_bkg <- BCCD(R, N, bkg_info = bkg_info)
#' print(bccd.fit1b_with_bkg$PAG) # Print adjacency matrix of inferred graph
#'
#' tryCatch({
#'   op <- par(mfrow = c(1, 2))
#'   plot_PAG(bccd.fit1b$PAG)
#'   plot_PAG(bccd.fit1b_with_bkg$PAG)
#' }, finally = par(op))
#'
#' # 1c. Generate data from a Y-structure (X -> Z <- Y & Z -> W)
#' N <- 10000 # number of observations
#' X <- rnorm(N)
#' Y <- rnorm(N)
#' Z <- X + Y + rnorm(N)
#' W <- Z + rnorm(N)
#' R <- cor(cbind(X, Y, Z, W), cbind(X, Y, Z, W))
#'
#' bccd.fit1c <- BCCD(R, N)
#' plot_PAG(bccd.fit1c$PAG) # plot inferred Y structure
#'
#' # 1d. Generate data from an almost Y-structure (X -> Z <- Y & Z, Y -> W)
#' N <- 10000 # number of observations
#' X <- rnorm(N)
#' Y <- rnorm(N)
#' Z <- X + Y + rnorm(N)
#' W <- Z + Y + rnorm(N)
#' R <- cor(cbind(X, Y, Z, W), cbind(X, Y, Z, W))
#'
#' bccd.fit1d <- BCCD(R, N)
#' plot_PAG(bccd.fit1d$PAG) # plot inferred almost Y-structure
#'
#' # 2. Generate MVN data from a fixed graph structure (G[i, j] == 1 <=> i -> j)
#' N <- 100 # number of observations
#' G <- matrix(
#' c(0, 0, 0, 0, 0,
#'   1, 0, 0, 0, 0,
#'   0, 0, 0, 0, 0,
#'   0, 1, 0, 0, 0,
#'   0, 1, 1, 0, 0), nrow = 5, ncol = 5, byrow = TRUE
#' )
#'
#' data <- t(solve(diag(5) - G) %*% matrix(rnorm(N * 5), 5, N))
#' bccd.fit2 <- BCCD(cor(data), N)
#'
#' print(bccd.fit2$PAG)
#' plot_PAG(bccd.fit2$PAG) # Plot resulting partial ancestral graph (PAG)
#'
#' # Add the (incorrect) background knowledge that 4 is adjacent to 5, but not to 2
#' bkg_info <- list(
#'   structural = matrix(c(
#'     0, 0, 0, 0, 0,
#'     0, 0, 0, -1, 0,
#'     0, 0, 0, 0, 0,
#'     0, -1, 0, 0, 1,
#'     0, 0, 0, 1, 0), nrow = 5, ncol = 5
#'   ),
#'   causal = NULL
#' )
#' bccd.fit2_with_bkg <- BCCD(cor(data), N, bkg_info = bkg_info)
#' print(bccd.fit2_with_bkg$PAG) # Print adjacency matrix of inferred graph
#' plot_PAG(bccd.fit2_with_bkg$PAG) # plot inferred PAG
#'
#' # 3. Generate a random inverse-Wishart distributed covariance matrix of size D
#' \dontrun{
#'   N <- 1000 # number of observations
#'   D <- 10 # number of variables / dimensions
#'   R <- cov2cor(solve(rWishart(1, D + 1, diag(D))[, , 1])) # inverse-Wishart
#'
#'   bccd.fit3 <- BCCD(R, N)
#'   plot_PAG(bccd.fit3$PAG) # Plot resulting partial ancestral graph (PAG)
#' }
#'
#'
#' @references
#'   \insertRef{claassen2012bayesian}{RUcausal}
#'
BCCD <- function(R, N, runmode = list_algorithm_parameters(), max_K = 5,
                 prior_BGe = NULL, bkg_info = NULL, no_selection_bias = FALSE, ASP = FALSE,
                 provide_detailed_output = FALSE, save_intermediate_output = FALSE) {

  D <- dim(R)[1] # number of variables / dimensions

  if (D < 2) stop("Input data must contain at least two variables.")
  
  # TODO: add check that R is a correlation matrix

  runmode$KK <- 2:min(D, max_K) # score subsets of size from 2 to min(D, max_K)

  if (is.null(prior_BGe)) {
    # Default prior BGe parameters for given size of data D
    prior_BGe <- list(
      alpha_mu = 1,
      Lambda = diag(D), # D is the number of variables / dimensions
      alpha_w = D + 1 # degrees of freedom > D - 1
    )
  }

  # TODO: find a better solution than checking every individual component
  if (is.null(bkg_info)) {
    bkg_info <- list(
      causal = matrix(0, D, D),
      structural = matrix(0, D, D)
    )
  } else if (is.null(bkg_info$causal)) {
    bkg_info$causal <- matrix(0, D, D)
  } else if (is.null(bkg_info$structural)) {
    bkg_info$structural <- matrix(0, D, D)
  }

  print(sprintf('%s === START BCCD', Sys.time()))

  # Call BCCD with BGe score ----------------------------------------------

  print(sprintf('%s === Deriving probabilities over logical causal statements', Sys.time()))

  # Translate background information into implied logical causal statements
  logic_prob_bkg <- translate_bkg_info(bkg_info$causal, no_selection_bias)

  # Compute probabilities of all logical causal statements
  logic_prob_BGe <- compute_probability_causal_logic_statements(R, N, prior_BGe, bkg_info, runmode)


  # Merge and filter list, run (B)LoCI ------------------------------------
  print(sprintf('%s === Running Bayesian LoCI (Logical Causal Inference)', Sys.time()))

  # Extract skeleton (and discard unused info from list to process) ?
  skeleton <- extract_skeleton(logic_prob_BGe, logic_prob_bkg, D, runmode$minp_loci, runmode$minp_PAG_edge)

  # Call LoCI on list pL_use
  if (ASP == TRUE) {
    loci_output <- run_bayesian_loci_ASP(skeleton, D, runmode)
  } else {
    loci_output <- run_bayesian_loci(skeleton$loci_puse, D, skeleton$graph, runmode)
  }



  # Update (override) skeleton with structural background information, if available
  if (!is.null(bkg_info$structural)) {
    skeleton$graph <- update_skeleton_with_bkg_info(skeleton$graph, bkg_info$structural)
  }


  # Produce BCCD Output ---------------------------------------------------

  print(sprintf('%s === Producing output', Sys.time()))

  # Convert to PAG representation
  PAG <- convert_causal_matrix_to_PAG(loci_output$Mc, skeleton$graph)

  # Get matrices containing edge / orientation probabilities (Table 3)
  reliability_tables <- produce_probability_tables(D, logic_prob_BGe, loci_output$trace_L)

  print(sprintf('%s === END BCCD', Sys.time()))

  # Save output
  # TODO: add other information such as the number of BCCD tests
  if (save_intermediate_output) {
    print(sprintf("Saving intermediate output to %s", runmode$output_file))
    save(logic_prob_bkg, logic_prob_BGe, skeleton, loci_output, PAG,
         reliability_tables, file = runmode$output_file)
  }

  if (provide_detailed_output) {
    list(
      PAG = PAG,
      causal_matrix = loci_output$Mc,
      reliability_tables = reliability_tables,
      prob_L_use = add_logic_statement_description(skeleton$loci_puse),
      prob_L_max = add_logic_statement_description(logic_prob_BGe),
      trace_L = loci_output$trace_L,
      trace_C = skeleton$trace
    )
  } else {
    list(
      PAG = PAG,
      causal_matrix = loci_output$Mc,
      reliability_tables = reliability_tables
    )
  }
}

